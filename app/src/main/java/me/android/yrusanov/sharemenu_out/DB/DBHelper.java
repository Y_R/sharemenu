package me.android.yrusanov.sharemenu_out.DB;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by y.rusanov on 2017-7-16.
 */

public class DBHelper extends SQLiteOpenHelper{


    public static final String DATABASE_NAME = "PostMenu.db";
    private String table;
    SQLiteDatabase db;
    Context context;
    public DBHelper(Context context, String _table) {
        super(context, DATABASE_NAME, null, 1);
        this.table = _table;
        this.context = context;
        open();
    }

    public DBHelper(Context context){
        super(context, DATABASE_NAME,null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+ Tables.TABLE_PRODUCT+" ("+Fields.ID+" "+ Types.INTEGER+" PRIMARY KEY AUTOINCREMENT, "+Fields.NAME+" "+Types.TEXT+", "+Fields.DESCRIPTION+" "+ Types.TEXT+", "+Fields.PRICE+" "+ Types.DOUBLE+", "+Fields.IDCATEGORY+" "+Types.INTEGER+", "+Fields.STATUS+" "+Types.INTEGER+")");
        db.execSQL("create table "+ Tables.TABLE_CATEGORY+" ("+Fields.ID+" "+ Types.INTEGER+" PRIMARY KEY AUTOINCREMENT, "+Fields.NAME+" "+Types.TEXT+")");
        db.execSQL("create table "+ Tables.TABLE_MENU+" ("+Fields.ID+" "+ Types.INTEGER+" PRIMARY KEY AUTOINCREMENT, "+Fields.NAME+" "+Types.TEXT+", "+Fields.DESCRIPTION+" "+ Types.TEXT+", "+Fields.PRICE+" "+ Types.DOUBLE+", "+Fields.IDCATEGORY+" "+Types.INTEGER+", "+Fields.STATUS+" "+Types.INTEGER+")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+ Tables.TABLE_PRODUCT);
        onCreate(db);
    }

    public void open() throws SQLException {
        db = this.getWritableDatabase();
    }

    public void close(){
        db.close();
    }

}

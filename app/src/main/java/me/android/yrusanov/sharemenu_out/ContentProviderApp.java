package me.android.yrusanov.sharemenu_out;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.util.HashMap;

import me.android.yrusanov.sharemenu_out.DB.DBHelper;
import me.android.yrusanov.sharemenu_out.DB.Tables;

public class ContentProviderApp extends ContentProvider {

    public static final String PROVIDER_NAME = "me.android.yrusanov.sharemenu_out.ContentProviderApp";
    public static final Uri CONTENT_URI_CATEGORY = Uri.parse("content://" + PROVIDER_NAME + "/"+ Tables.TABLE_CATEGORY+"");
    public static final Uri CONTENT_URI_PRODUCT = Uri.parse("content://" + PROVIDER_NAME + "/"+Tables.TABLE_PRODUCT+"");
    public static final Uri CONTENT_URI_MENU= Uri.parse("content://" + PROVIDER_NAME + "/"+Tables.TABLE_MENU+"");
    public static final String and_cursor = "vnd.android.cursor.dir/";
    ContentObserverApp contentObserverApp;
    static final int CATEGORY = 1;
    static final int PRODUCT = 2;
    static final int MENU = 3;

    private static HashMap<String, String> values;
    static  final UriMatcher uriMatcher;
    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_PRODUCT,PRODUCT);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_CATEGORY,CATEGORY);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_MENU,MENU);
    }
    private SQLiteDatabase sqlDB;
    private DBHelper dbHelper;

    @Override
    public boolean onCreate() {
        dbHelper = new DBHelper(getContext());
        sqlDB = dbHelper.getWritableDatabase();
        if(sqlDB != null){
            return true;
        }
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        Cursor cursor = null;

        switch (uriMatcher.match(uri)){
            case CATEGORY:
                queryBuilder.setTables(Tables.TABLE_CATEGORY);
                break;
            case PRODUCT:
                queryBuilder.setTables(Tables.TABLE_PRODUCT);
                break;
            case MENU:
                queryBuilder.setTables(Tables.TABLE_MENU);
                break;
            default:
                throw new IllegalArgumentException("Unkwon URI "+uri);
        }
        queryBuilder.setProjectionMap(values);
        int cnt;
        if(selection != null){
            Cursor cursorCount = sqlDB.rawQuery("SELECT * FROM "+queryBuilder.getTables()+" WHERE "+selection+"",null);
            cnt = cursorCount.getCount();
            cursorCount.close();
        }
        else{
            Cursor cursorCount = sqlDB.rawQuery("SELECT * FROM "+queryBuilder.getTables()+"",null);
            cnt = cursorCount.getCount();
            cursorCount.close();
        }
        getContext().getContentResolver().notifyChange(uri,contentObserverApp);
        if(cnt != 0){
            cursor = queryBuilder.query(sqlDB, projection, selection, selectionArgs, null,null,sortOrder);
            //cursor.setNotificationUri(getContext().getContentResolver(),uri);
            return cursor;
        }
        else{
            return null;
        }
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)){
            case CATEGORY:
                return and_cursor+Tables.TABLE_CATEGORY;
            case PRODUCT:
                return and_cursor+Tables.TABLE_PRODUCT;
            case MENU:
                return and_cursor+Tables.TABLE_MENU;
            default:
                throw new IllegalArgumentException("Unsupported URI "+uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {

        long rowID;
        Uri _uri;

        switch (uriMatcher.match(uri)) {
            case CATEGORY:
                rowID = sqlDB.insert(Tables.TABLE_CATEGORY, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_CATEGORY, rowID);
                break;
            case PRODUCT:
                rowID = sqlDB.insert(Tables.TABLE_PRODUCT, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_PRODUCT, rowID);
                break;
            case MENU:
                rowID = sqlDB.insert(Tables.TABLE_MENU, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_MENU, rowID);
                break;
            default:
                throw new IllegalArgumentException("Unkwon URI " + uri);
        }

        if(rowID > 0){
            getContext().getContentResolver().notifyChange(_uri,contentObserverApp);
            return _uri;
        }
        else{
            Toast.makeText(getContext(), "Row Inserted Failed",Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)){
            case PRODUCT:
                count = sqlDB.delete(Tables.TABLE_PRODUCT, selection, selectionArgs);
                break;
            case MENU:
                count = sqlDB.delete(Tables.TABLE_MENU, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, contentObserverApp);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int count = 0;
            switch (uriMatcher.match(uri)) {
                case PRODUCT:
                    count = sqlDB.update(Tables.TABLE_PRODUCT, values, selection, selectionArgs);
                    break;
                case MENU:
                    count = sqlDB.update(Tables.TABLE_MENU, values, selection, selectionArgs);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown URI " + uri );
            }
            getContext().getContentResolver().notifyChange(uri, contentObserverApp);
            return count;
    }
}

package me.android.yrusanov.sharemenu_out.DB;

/**
 * Created by y.rusanov on 2017-7-16.
 */

public class Fields {
    public static final String ID = "ID";
    public static final String NAME = "NAME";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String PRICE = "PRICE";
    public static final String IDCATEGORY = "IDCATEGOTY";
    public static final String STATUS = "STATUS";

}

package me.android.yrusanov.sharemenu_out.layout;


import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import me.android.yrusanov.sharemenu_out.ContentProviderApp;
import me.android.yrusanov.sharemenu_out.DB.Fields;
import me.android.yrusanov.sharemenu_out.R;
import me.android.yrusanov.sharemenu_out.tables.Category;
import me.android.yrusanov.sharemenu_out.tables.Product;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddFragment extends Fragment {

    ImageButton addProduct;
    ImageButton addCategory;
    Product product;
    Category category;
    EditText editText_Product, editText_Category, editText_Price;
    ContentResolver resolver;
    Spinner spinner_category;
    boolean inserted;

    public AddFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add, container, false);
        spinner_category = (Spinner) view.findViewById(R.id.spinner_addCategoy);


        this.populateSpinner(view);
        this.addCategoryClick(view);
        this.addProductClick(view);

        return view;
    }


    public void populateSpinner(View view){
        //populate spinner
        ArrayAdapter<String> spinner_array = new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_spinner_dropdown_item);

        String[] queryCols=new String[]{"NAME"};
        Cursor cursor_category = view.getContext().getContentResolver().query(
                ContentProviderApp.CONTENT_URI_CATEGORY,
                queryCols,
                null,
                null,
                null
        );
        if(cursor_category != null){
            cursor_category.moveToFirst();
            do {
                spinner_array.add(cursor_category.getString(0));
            } while (cursor_category.moveToNext());
        }
        spinner_category.setAdapter(spinner_array);
        //<--

    }

    private void addCategoryClick(View view){
        editText_Category = (EditText)view.findViewById(R.id.editText_Category);

        addCategory = (ImageButton) view.findViewById(R.id.imageButton_addCategory);
        addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues mNewValues = new ContentValues();
                category = new Category();
                category.setName(editText_Category.getText().toString());
                if(category.getName().equals("")){
                    Toast.makeText(v.getContext(),"Category does empty",Toast.LENGTH_SHORT).show();
                }
                else {
                    int categoryId = 0;
                    String whereClause = ""+ Fields.NAME+" = '"+category.getName()+"'";
                    Cursor cursor_category;
                    cursor_category = v.getContext().getContentResolver().query(ContentProviderApp.CONTENT_URI_CATEGORY,null,whereClause,null,null);
                    if(cursor_category != null) {
                        cursor_category.moveToFirst();
                        categoryId = cursor_category.getInt(0);
                    }
                    if(categoryId != 0){
                        Toast.makeText(v.getContext(),"Category exists",Toast.LENGTH_SHORT).show();
                    }
                    else {
                        mNewValues.put(Fields.NAME, category.getName());
                        v.getContext().getContentResolver().insert(ContentProviderApp.CONTENT_URI_CATEGORY, mNewValues);
                        Toast.makeText(v.getContext(),"Category inserted",Toast.LENGTH_SHORT).show();
                        populateSpinner(v);
                    }
                }
            }
        });
    }

    private void addProductClick(View view){
        addProduct = (ImageButton) view.findViewById(R.id.imageButton_addProduct);
        editText_Product = (EditText)view.findViewById(R.id.editText_Product);
        editText_Price = (EditText)view.findViewById(R.id.editText_price);

        addProduct.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ContentValues mNewValues = new ContentValues();

                if(spinner_category.getSelectedItem() == null){
                    Toast.makeText(v.getContext(),"Category does not selecetd",Toast.LENGTH_SHORT).show();
                }
                else{
                    int productId = 0;
                    String whereClause = ""+ Fields.NAME+" = '"+editText_Product.getText().toString()+"'";
                    Cursor cursor_productId;
                    cursor_productId = v.getContext().getContentResolver().query(ContentProviderApp.CONTENT_URI_PRODUCT,null,whereClause,null,null);
                    if(cursor_productId != null) {
                        cursor_productId.moveToFirst();
                        productId = cursor_productId.getInt(0);
                    }
                    else if(productId != 0){
                        Toast.makeText(v.getContext(),"Product exists", Toast.LENGTH_LONG).show();
                    }
                    else if(editText_Price.getText().toString().equals("")){
                        Toast.makeText(v.getContext(),"Price its empty", Toast.LENGTH_LONG).show();
                    }
                    else if(editText_Product.getText().toString().equals("")){
                        Toast.makeText(v.getContext(),"Product its empty", Toast.LENGTH_LONG).show();
                    }
                    else{
                        String whereClauseCategory = ""+ Fields.NAME+" = '"+spinner_category.getSelectedItem().toString()+"'";
                        Cursor cursor_Category;
                        cursor_Category = v.getContext().getContentResolver().query(ContentProviderApp.CONTENT_URI_CATEGORY,null,whereClauseCategory,null,null);
                        int categoryId = 0;
                        if(cursor_Category != null) {
                            cursor_Category.moveToFirst();
                            categoryId = cursor_Category.getInt(0);
                        }
                        mNewValues.put(Fields.NAME, editText_Product.getText().toString());
                        mNewValues.put(Fields.IDCATEGORY, categoryId);
                        mNewValues.put(Fields.PRICE, Double.valueOf(editText_Price.getText().toString()));
                        v.getContext().getContentResolver().insert(ContentProviderApp.CONTENT_URI_PRODUCT, mNewValues);
                        Toast.makeText(v.getContext(),"Product inserted",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}

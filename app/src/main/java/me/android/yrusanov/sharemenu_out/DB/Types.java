package me.android.yrusanov.sharemenu_out.DB;

/**
 * Created by y.rusanov on 2017-7-16.
 */

public class Types {
    public static final String INTEGER = "INTEGER";
    public static final String TEXT = "TEXT";
    public static final String BLOB = "BLOB";
    public static final String DOUBLE = "DOUBLE";
    public static final String DATE = "DATE";
}

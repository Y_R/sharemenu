package me.android.yrusanov.sharemenu_out;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;

/**
 * Created by y.rusanov on 2017-8-3.
 */

public class ContentObserverApp extends ContentObserver {
    public ContentObserverApp(Handler handler) {
        super(handler);
    }

    public ContentObserverApp() {
        super(new Handler());
    }
    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange, null);
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
    }
}

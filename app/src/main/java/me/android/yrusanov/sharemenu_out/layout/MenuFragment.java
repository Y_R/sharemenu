package me.android.yrusanov.sharemenu_out.layout;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import me.android.yrusanov.sharemenu_out.ContentProviderApp;
import me.android.yrusanov.sharemenu_out.R;
import me.android.yrusanov.sharemenu_out.RecyclerAdapter.RecyclerAdapterMenu;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment {


    //ImageButton setList;
    Button setList;
    RecyclerView recyclerView;
    RecyclerAdapterMenu adapter;
    Boolean editMode;
    public MenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_menu);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new RecyclerAdapterMenu(this.getActivity().getBaseContext());
        recyclerView.setAdapter(adapter);
        Bundle bundle = this.getArguments();
        if(bundle != null){
            editMode = bundle.getBoolean("editMode",false);
        }
        this.setListClick(view);

        return view;
    }

    private void setListClick(View view){
        setList = (Button) view.findViewById(R.id.imageButton_setList);
        setList.setOnClickListener(new View.OnClickListener() {

            private void deleteRecords(){
                getContext().getContentResolver().delete(ContentProviderApp.CONTENT_URI_MENU,null,null);
            }

            @Override
            public void onClick(View v) {
                deleteRecords();
                CreateFragment createFragment = new CreateFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("editMode",editMode);
                createFragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction2 = getFragmentManager().beginTransaction();
                fragmentTransaction2.replace(R.id.content, createFragment);
                fragmentTransaction2.commit();
            }
        });
    }

}

package me.android.yrusanov.sharemenu_out.render;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.HashSet;

import me.android.yrusanov.sharemenu_out.ContentProviderApp;
import me.android.yrusanov.sharemenu_out.DB.Fields;
import me.android.yrusanov.sharemenu_out.R;
import me.android.yrusanov.sharemenu_out.tables.Menu;

/**
 * Created by y.rusanov on 2017-7-21.
 */

public class MenuRender {

    Bitmap bitmap;
    Canvas canvas;
    public static final int WIDTH = 600;
    public static final int HEIGHT = 1024;
    Context context;
    ArrayList<Menu> arrayList;
    String currency;
    HashSet<Integer> categoryHashSet;
    public MenuRender(Context context){
        arrayList = new ArrayList<>();
        categoryHashSet = new HashSet<>();
        this.context = context;
        currency = context.getResources().getString(R.string.currency);

        this.create();
    }

    private void create(){
        canvas = new Canvas(createBitmap(WIDTH, HEIGHT));
        this.read();
        this.rend();
    }

    private Bitmap createBitmap(int width, int height){
        Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
        bitmap = Bitmap.createBitmap(width, height, conf); // this creates a MUTABLE bitmap
        bitmap.eraseColor(Color.WHITE);
        return bitmap;
    }

    private void read(){
        //arrayList = getMenu();
        arrayList = getMenuProduct();
    }

    private String getCategory(int id){
        String whereClause = ""+ Fields.ID+" = "+id+"";
        Cursor cursor = context.getContentResolver().query(ContentProviderApp.CONTENT_URI_CATEGORY,null, whereClause, null,null);
        if(cursor != null){
            cursor.moveToFirst();
            return cursor.getString(1);
        }
        return null;
    }

    private ArrayList<Menu> getMenu(){
        ArrayList<Menu> menuArrayList = new ArrayList<>();
        Cursor cursor = context.getContentResolver().query(ContentProviderApp.CONTENT_URI_MENU,null,null,null,null);
        if(cursor != null){
            cursor.moveToFirst();
            do {
                menuArrayList.add(new Menu(cursor.getString(1), cursor.getDouble(3), cursor.getInt(4), cursor.getInt(5)));
                categoryHashSet.add(cursor.getInt(4));
            } while (cursor.moveToNext());
        }
        return menuArrayList;
    }

    private ArrayList<Menu> getMenuProduct(){
        ArrayList<Menu> menuArrayList = new ArrayList<>();
        String whereClause = ""+Fields.STATUS+" = 1";
        Cursor cursor = context.getContentResolver().query(ContentProviderApp.CONTENT_URI_PRODUCT,null,whereClause,null,null);
        if(cursor != null){
            cursor.moveToFirst();
            do {
                menuArrayList.add(new Menu(cursor.getString(1), cursor.getDouble(3), cursor.getInt(4), cursor.getInt(5)));
                categoryHashSet.add(cursor.getInt(4));
            } while (cursor.moveToNext());
        }
        return menuArrayList;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void rend(){
        float y = 50;
        y = drawHeader(y);
        y += 50;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            y = drawLines(y);
        }
        y += 50;
        y = drawFooter(y);
    }

    private float drawHeader(float y){

        Paint plogo =new Paint();
        Bitmap bLogo= BitmapFactory.decodeResource(context.getResources(), R.drawable.restlogo);
        //plogo.setColor(Color.RED);
        canvas.drawBitmap(bLogo, WIDTH/2 - bLogo.getWidth()/2, y, plogo);
        y = bLogo.getHeight() + 100;

        String subTitle = context.getResources().getString(R.string.companyName);
        Paint subPaint = paintSubTitle();
        canvas.drawText(subTitle, centerWidthText(subPaint, subTitle), y, paintSubTitle());

        y += 50;

        String title = context.getResources().getString(R.string.lunchMenuTitle);
        Paint paint = paintTitle();
        canvas.drawText(title, centerWidthText(paint, title), y, paintTitle());

        return y;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private float drawLines(float y){
        for (int category : categoryHashSet){
            canvas.drawText(getCategory(category), centerWidthText(paintSubTitle(),getCategory(category)),y, paintSubTitle());
            for (Menu menu : arrayList){
                if(menu.getIdCategory() == category){
                    y += 30;
                    String item = menu.getName() + " - " + menu.getPrice() + " " + currency;
                    canvas.drawText(item, centerWidthText(paintItem(),item  ),y,paintItem());
                }
            }
            y += 50;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            bitmap.setHeight(Math.round(y + 100));
        }

        return y;
    }

    private float drawFooter(float y){
        return y;
    }

    public Bitmap getBitmap(){
        return bitmap;
    }

    private Paint paintSubTitle(){
        Paint paint = new Paint();
        paint.setTextSize(26); // Text Size
        //paint.setColor(Color.rgb(242, 141, 129));
        paint.setColor(Color.BLACK);
        //paint.setTypeface(Typeface.create("Cambria", Typeface.BOLD));
        paint.setTypeface(Typeface.create("sans-serif-light", Typeface.BOLD));
        paint.setAntiAlias(true);
        //paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)); // Text Overlapping Pattern
        return paint;
    }

    private Paint paintTitle(){
        Paint paint = new Paint();
        paint.setTextSize(48); // Text Size
        //paint.setColor(Color.rgb(73, 44, 63));
        paint.setColor(Color.BLACK);
        //paint.setTypeface(Typeface.create("Cambria", Typeface.BOLD));
        paint.setTypeface(Typeface.create("sans-serif-light", Typeface.BOLD));
        paint.setAntiAlias(true);
        //paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)); // Text Overlapping Pattern
        return paint;
    }

    private Paint paintItem(){
        Paint paint = new Paint();
        paint.setTextSize(24); // Text Size
        //paint.setColor(Color.rgb(73, 44, 63));
        paint.setColor(Color.BLACK);
        //paint.setTypeface(Typeface.create("Cambria", Typeface.BOLD));
        paint.setTypeface(Typeface.create("sans-serif-light", Typeface.BOLD));
        paint.setAntiAlias(true);
        //paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)); // Text Overlapping Pattern
        return paint;
    }

    private float centerWidthText(Paint _paint, String text){
        Rect bounds = new Rect();
        _paint.getTextBounds(text, 0, text.length(), bounds);
        int width = bounds.width();
        return WIDTH/2 - width/2;
    }

    private float centerHeightText(Paint _paint, String text){
        Rect bounds = new Rect();
        _paint.getTextBounds(text, 0, text.length(), bounds);
        int height = bounds.height();
        return HEIGHT/2 - height/2;
    }
}
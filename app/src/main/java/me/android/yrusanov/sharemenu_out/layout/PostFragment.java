package me.android.yrusanov.sharemenu_out.layout;


import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import me.android.yrusanov.sharemenu_out.R;
import me.android.yrusanov.sharemenu_out.render.MenuRender;
import me.android.yrusanov.sharemenu_out.restFB.FBPost;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostFragment extends Fragment {


    ImageButton post;
    //Button post;
    ImageView imageView;
    Bitmap imageUpd;
    EditText editTextMessage;
    MenuRender menuRender;
    ProgressBar progressBar;
    public PostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_post, container, false);
        imageView = (ImageView)view.findViewById(R.id.imageView);
        editTextMessage = (EditText)view.findViewById(R.id.editText_post_message);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar_post);

        menuRender = new MenuRender(getContext());
        imageView.setImageBitmap(menuRender.getBitmap());
        this.postClick(view);
        return view;
    }

    private void postClick(final View view){
        post = (ImageButton) view.findViewById(R.id.imageButton_post);
        post.setOnClickListener(new View.OnClickListener() {

            private boolean isInternetAvailable() {
                ConnectivityManager cm =
                        (ConnectivityManager) view.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                return netInfo != null && netInfo.isConnectedOrConnecting();
            }

            @Override
            public void onClick(View v) {
                imageUpd = menuRender.getBitmap();
                progressBar.setVisibility(View.VISIBLE);

                if(this.isInternetAvailable()){
                    FBPost fbPost = new FBPost();
                    fbPost.startThread(imageUpd,editTextMessage.getText().toString(),v.getContext(),progressBar);
                }
                else {
                    Toast.makeText(v.getContext(), "Check your Internet connection", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}

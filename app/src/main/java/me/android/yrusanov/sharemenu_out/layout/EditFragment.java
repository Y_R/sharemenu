package me.android.yrusanov.sharemenu_out.layout;


import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import me.android.yrusanov.sharemenu_out.ContentProviderApp;
import me.android.yrusanov.sharemenu_out.DB.Fields;
import me.android.yrusanov.sharemenu_out.R;
import me.android.yrusanov.sharemenu_out.tables.Product;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditFragment extends Fragment {


    public EditFragment() {
        // Required empty public constructor
    }


    Button editBut, deleteBut, backBut;
    EditText productName, productPrice;
    TextView productCstegory;
    Product product;
    Boolean editMode;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit, container, false);
        productName = (EditText)view.findViewById(R.id.editText_edit_name);
        productCstegory = (TextView)view.findViewById(R.id.textView_edit_category);
        productPrice = (EditText) view.findViewById(R.id.editText_edit_price);
        Bundle bundle = this.getArguments();
        //editMode = bundle.getBoolean("editMode",false);
        int myInt = bundle.getInt("ID", -1);
        if(myInt != -1){
            Cursor cursor = view.getContext().getContentResolver().query(ContentProviderApp.CONTENT_URI_PRODUCT,null, Fields.ID + " = " + myInt, null, null);
            cursor.moveToFirst();
            productName.setText(cursor.getString(1));
            productPrice.setText(String.valueOf(cursor.getDouble(3)));
            Cursor c = view.getContext().getContentResolver().query(ContentProviderApp.CONTENT_URI_CATEGORY, null, Fields.ID + " = " + cursor.getInt(4),null,null);
            c.moveToFirst();
            productCstegory.setText(c.getString(1));

            product = new Product(cursor.getInt(0),cursor.getString(1), cursor.getDouble(3), cursor.getInt(4), cursor.getInt(5));

            c.close();
            cursor.close();
        }
        this.editClick(view);
        this.deleteClick(view);

        return view;
    }

    private void editClick(View view) {
        editBut = (Button) view.findViewById(R.id.button_edit_save);
        editBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!product.getName().equals(productName.getText().toString())
                        || product.getPrice() != Double.valueOf(productPrice.getText().toString())){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(Fields.NAME, productName.getText().toString());
                    contentValues.put(Fields.PRICE, Double.valueOf(productPrice.getText().toString()));
                    v.getContext().getContentResolver().update(ContentProviderApp.CONTENT_URI_PRODUCT,contentValues,Fields.ID + " = " + product.getId(),null);

                    CreateFragment createFragment = new CreateFragment();
                    FragmentTransaction fragmentTransaction2 = getFragmentManager().beginTransaction();
                    fragmentTransaction2.replace(R.id.content, createFragment);
                    fragmentTransaction2.commit();


                }
            }
        });
    }

    private void deleteClick(View view) {
        deleteBut = (Button) view.findViewById(R.id.button_edit_delete);
        deleteBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}

package me.android.yrusanov.sharemenu_out.layout;


import android.content.ContentValues;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;

import me.android.yrusanov.sharemenu_out.ContentObserverApp;
import me.android.yrusanov.sharemenu_out.ContentProviderApp;
import me.android.yrusanov.sharemenu_out.DB.Fields;
import me.android.yrusanov.sharemenu_out.R;
import me.android.yrusanov.sharemenu_out.RecyclerAdapter.RecyclerAdapterProduct;
import me.android.yrusanov.sharemenu_out.cursorAdapter.CursorAdapterProduct;
import me.android.yrusanov.sharemenu_out.cursorAdapter.CursorAdapterProduct12;
import me.android.yrusanov.sharemenu_out.tables.Product;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<Cursor>
        , SearchView.OnQueryTextListener {

    ContentObserverApp contentObserverApp;

    RecyclerView recyclerView;
    RecyclerAdapterProduct adapter;
    CheckBox checkBox;
    ArrayAdapter<Product> arrayAdapter;
    View view;
    CursorAdapterProduct12 cursorAdapterProduct1;
    //ImageButton checkList;//, clearList;
    Button checkList,clearList, editItem,editButton;
    FloatingActionButton editBut, addBut, clearBut, editItemBut;
    LoaderManager.LoaderCallbacks callbacks;
    CursorAdapterProduct cursorAdapterProduct;
    Animation fabOpen, fabClose, fabItemOpen, fabItemClose;
    boolean isOpen=false;

    ImageView imageView;

    public CreateFragment() {
        // Required empty public constructor
    }

    private Cursor getCursorFitlerByCategory(){
        return getContext().getContentResolver().query(ContentProviderApp.CONTENT_URI_PRODUCT,null,null,null,Fields.IDCATEGORY + " ASC");
    }


    private boolean productRecNull(){
        Cursor c = this.getContext().getContentResolver().query(ContentProviderApp.CONTENT_URI_PRODUCT, null,null,null,null);

        if(c != null){
            return false;
        }
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_create, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_product);
        editBut = (FloatingActionButton)view.findViewById(R.id.edit_product);
        addBut = (FloatingActionButton)view.findViewById(R.id.add_product);
        editItemBut = (FloatingActionButton)view.findViewById(R.id.edit_item);
        clearBut = (FloatingActionButton)view.findViewById(R.id.clear_product);;

        imageView = (ImageView)view.findViewById(R.id.imageView3);

        if(this.productRecNull()){
            imageView.setVisibility(View.VISIBLE);
        }
        else {
            imageView.setVisibility(View.GONE);
        }

        clearBut.hide();
        addBut.hide();
        editItemBut.hide();
        recyclerView.setHasFixedSize(true);
        checkBox = (CheckBox)view.findViewById(R.id.checkBox);
        setHasOptionsMenu(true);
        //editButton.setVisibility(View.GONE);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //adapter = new RecyclerAdapterProduct(this.getActivity().getBaseContext());
        //recyclerView.setAdapter(adapter);
        //fabOpen = AnimationUtils.loadAnimation(getContext().getApplicationContext(),R.anim.rotate1);
        //fabClose = AnimationUtils.loadAnimation(getContext().getApplicationContext(),R.anim.rotate2);
        cursorAdapterProduct = new CursorAdapterProduct(getCursorFitlerByCategory(),view.getContext(),getFragmentManager());
        recyclerView.setAdapter(cursorAdapterProduct);
        callbacks = this;
        getLoaderManager().initLoader(0,null,callbacks);
        this.checkListClick(view);
        this.clearListClick(view);
        this.editListClick(view);
        this.addItemClick(view);
        this.editItemClick(view);
        cursorAdapterProduct.fragmentManager = getFragmentManager();
        getLoaderManager().restartLoader(0,null,callbacks);
        /*adapter.SetOnItemClickListener( new RecyclerAdapterProduct.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }
        });
*/
        //editMode();
        //cursorAdapterProduct.notifyItemRangeChanged(0, cursorAdapterProduct.getItemCount());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0){
                    isOpen = false;
                    editBut.hide();
                    clearBut.hide();
                    editItemBut.hide();
                    addBut.hide();
                }else{
                    editBut = setFloatingActionButton(editBut,getColorStateListBeforeClick("#FFFFFF","#8e8e8e"),R.drawable.ic_keyboard_arrow_up_white_24dp);
                    editBut.show();
                }
            }
        });
        return view;
    }

    private void clearListClick(View view){
        clearList = (Button) view.findViewById(R.id.imageButton_clearList);

        clearBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(Fields.STATUS, 0);
                getContext().getContentResolver().update(ContentProviderApp.CONTENT_URI_PRODUCT,contentValues,null,null);
                //cursorAdapterProduct.notifyDataSetChanged();
                getLoaderManager().restartLoader(0,null,callbacks);
            }
        });
    }

    private void addItemClick(View view){
        addBut.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                AddFragment addFragment = new AddFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                getFragmentManager().popBackStack();
                fragmentTransaction.replace(R.id.content, addFragment).addToBackStack("tag");
                fragmentTransaction.commit();
            }
        });
    }

    private void editItemClick(View view){
        editItemBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!cursorAdapterProduct.editMode)
                    cursorAdapterProduct.editMode = true;
                else
                    cursorAdapterProduct.editMode = false;

                cursorAdapterProduct.notifyDataSetChanged();
            }
        });
    }

    private void checkListClick(View view){
        checkList = (Button) view.findViewById(R.id.imageButton_checkList);
        checkList.setOnClickListener(new View.OnClickListener() {

            private void insertMenu(){
                Product product;
                Cursor cursor = getContext().getContentResolver().query(ContentProviderApp.CONTENT_URI_PRODUCT,null,"STATUS = 1",null,null);
                if(cursor != null){
                    cursor.moveToFirst();
                    do {
                        product = new Product(cursor.getString(1), cursor.getDouble(3), cursor.getInt(4), cursor.getInt(5));
                        insertMenuProduct(product);
                    } while (cursor.moveToNext());
                }
            }

            private void deleteRecords(){
                getContext().getContentResolver().delete(ContentProviderApp.CONTENT_URI_MENU,null,null);
            }

            private  void insertMenuProduct(Product product){
                ContentValues contentValues = new ContentValues();
                contentValues.put(Fields.NAME, product.getName());
                contentValues.put(Fields.PRICE, product.getPrice());
                contentValues.put(Fields.STATUS, product.isCheck());
                contentValues.put(Fields.IDCATEGORY, product.getIdCategory());
                getContext().getContentResolver().insert(ContentProviderApp.CONTENT_URI_MENU, contentValues);
            }

            @Override
            public void onClick(View v) {
                this.deleteRecords();
                this.insertMenu();
                MenuFragment menuFragment= new MenuFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content, menuFragment);
                fragmentTransaction.commit();
            }
        });
    }

    String mCurFilter;

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String likeClause = ""+Fields.NAME+" LIKE '%"+mCurFilter+"%'";
        Cursor cursor = getContext().getContentResolver().query(ContentProviderApp.CONTENT_URI_PRODUCT, null, likeClause,null,null);

        if(mCurFilter == null) {
            return new CursorLoader(getActivity(), ContentProviderApp.CONTENT_URI_PRODUCT, null, null, null, Fields.IDCATEGORY + " ASC");
        }
        else if(cursor == null){
            cursorAdapterProduct.swapCursor(null);
            cursorAdapterProduct.notifyDataSetChanged();
            return null;
        }
        else {
            return new CursorLoader(getActivity(),ContentProviderApp.CONTENT_URI_PRODUCT ,null,likeClause,null,null);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        cursorAdapterProduct.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        cursorAdapterProduct.swapCursor(null);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Place an action bar item for searching.
        MenuItem item = menu.add("Search");
        item.setIcon(android.R.drawable.ic_menu_search);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        SearchView sv = new SearchView(getActivity());
        sv.setOnQueryTextListener(this);
        item.setActionView(sv);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mCurFilter = !TextUtils.isEmpty(newText) ? newText : null;
        getLoaderManager().restartLoader(0,null,CreateFragment.this);
        return true;
    }

    private void editMode(){
        cursorAdapterProduct.setCategoryName("");
        if (isOpen){
            editBut = setFloatingActionButton(editBut,getColorStateListBeforeClick("#FFFFFF","#8e8e8e"),R.drawable.ic_keyboard_arrow_up_white_24dp);
            clearBut.hide();
            addBut.hide();
            editItemBut.hide();
            //editBut.setAnimation(fabClose);
            isOpen=false;
        }
        else {
            editBut = setFloatingActionButton(editBut, getColorStateListAfterClick("#8e8e8e","#FFFFFF"), R.drawable.ic_keyboard_arrow_up_black_24dp);
            clearBut.show();
            addBut.show();
            editItemBut.show();
            //editBut.setAnimation(fabOpen);
            isOpen=true;
        }
    }

    private void editListClick(View view){
        editBut = (FloatingActionButton) view.findViewById(R.id.edit_product);
        editBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editMode();
                //getLoaderManager().restartLoader(0,null,callbacks);
            }
        });
    }

    public ColorStateList getColorStateListAfterClick(String hexColor1, String hexColor2){
        return new ColorStateList(
                new int[][]{new int[]{android.R.attr.state_pressed},
                        new int[]{}
                },
                new int[]{Color.parseColor(hexColor1),Color.parseColor(hexColor2)});
    }

    public ColorStateList getColorStateListBeforeClick(String hexColor1, String hexColor2){
        return new ColorStateList(
                new int[][]{new int[]{android.R.attr.state_pressed},
                        new int[]{}
                },
                new int[]{Color.parseColor(hexColor1),Color.parseColor(hexColor2)});
    }

    public FloatingActionButton setFloatingActionButton(FloatingActionButton floatingActionButton, ColorStateList colorStateList, int imageResourceId){
        floatingActionButton.setBackgroundTintList(colorStateList);
        floatingActionButton.setImageResource(imageResourceId);
        return floatingActionButton;
    }
}
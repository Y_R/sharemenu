package me.android.yrusanov.sharemenu_out;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import me.android.yrusanov.sharemenu_out.layout.CreateFragment;
import me.android.yrusanov.sharemenu_out.layout.PostFragment;

public class MainActivity extends AppCompatActivity
implements FragmentManager.OnBackStackChangedListener {

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            getSupportFragmentManager().popBackStack();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    /*
                    AddFragment addFragment = new AddFragment();
                    FragmentTransaction fragmentTransaction1 = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction1.replace(R.id.content, addFragment);
                    shouldDisplayHomeUp();
                    fragmentTransaction1.commit();
                    return true;
                    */
                    CreateFragment createFragment = new CreateFragment();
                    FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction2.replace(R.id.content, createFragment);
                    shouldDisplayHomeUp();
                    fragmentTransaction2.commit();
                    return true;
                //case R.id.navigation_dashboard:
                    /*
                    CreateFragment createFragment = new CreateFragment();
                    FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction2.replace(R.id.content, createFragment);
                    shouldDisplayHomeUp();
                    fragmentTransaction2.commit();
                    return true;
                    */
                case R.id.navigation_notifications:
                    PostFragment postFragment = new PostFragment();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.content, postFragment);
                    shouldDisplayHomeUp();
                    fragmentTransaction.commit();
                    return true;
            }
            return false;
        }

    };
    ContentObserverApp contentObserverApp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        getSupportFragmentManager().addOnBackStackChangedListener(this);
        //Handle when activity is recreated like on orientation Change
        shouldDisplayHomeUp();

        CreateFragment createFragment = new CreateFragment();
        FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
        fragmentTransaction2.replace(R.id.content, createFragment);
        shouldDisplayHomeUp();
        fragmentTransaction2.commit();
        Handler handler = null;
        getContentResolver().registerContentObserver(ContentProviderApp.CONTENT_URI_PRODUCT,true,new ContentObserverApp(handler));
    }

    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
        getSupportFragmentManager().popBackStack();
        return true;
    }

    public void shouldDisplayHomeUp(){
        //Enable Up button only  if there are entries in the back stack
        boolean canback = getSupportFragmentManager().getBackStackEntryCount()>0;
        getSupportActionBar().setDisplayHomeAsUpEnabled(canback);
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }
}

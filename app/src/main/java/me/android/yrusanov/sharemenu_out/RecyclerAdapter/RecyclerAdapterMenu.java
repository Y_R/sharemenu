package me.android.yrusanov.sharemenu_out.RecyclerAdapter;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import me.android.yrusanov.sharemenu_out.ContentProviderApp;
import me.android.yrusanov.sharemenu_out.DB.Fields;
import me.android.yrusanov.sharemenu_out.R;
import me.android.yrusanov.sharemenu_out.tables.Menu;

/**
 * Created by y.rusanov on 2017-7-21.
 */

public class RecyclerAdapterMenu extends RecyclerView.Adapter<RecyclerAdapterMenu.ElementViewHolder>{

    OnItemClickListener mItemClickListener;
    ContentResolver resolver;
    ArrayList<Menu> elements;
    Menu menu;
    String path;
    String category = "";
    public String language;

    static final Uri CONTENT_URL = Uri.parse(String.valueOf(ContentProviderApp.CONTENT_URI_MENU));

    public RecyclerAdapterMenu(Context _context){

        elements = new ArrayList<>();
        String contentName;
        try {
            //elements = populateBeer.selectAllByCategory(_IdCategory);
            //elements = populateBeer.populateAll();
            resolver = _context.getContentResolver();
            Cursor cursor = resolver.query(CONTENT_URL,null,null,null,Fields.IDCATEGORY + " ASC");
            if(cursor != null){
                cursor.moveToFirst();
                do {
                    elements.add(new Menu(cursor.getString(1), cursor.getDouble(3), cursor.getInt(4), cursor.getInt(5)));
                } while (cursor.moveToNext());
            }

        }
        catch(ArrayIndexOutOfBoundsException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public ElementViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_menu, parent, false);
        return new ElementViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(ElementViewHolder holder, int position) {

        if(elements != null) {
            menu = (Menu) elements.get(position);
            holder.textViewName.setText(menu.getName());
            holder.textViewPrice.setText(String.valueOf(menu.getPrice()));
            String[] projection = {Fields.NAME};
            Cursor c = resolver.query(ContentProviderApp.CONTENT_URI_CATEGORY,projection,""+ Fields.ID+" = "+elements.get(position).getIdCategory()+"",null,null);
            c.moveToFirst();

            if(category.equals(c.getString(c.getColumnIndex(Fields.NAME)))){
                holder.textVIewCategory.setText("");
                holder.categoryLayout.setVisibility(View.GONE);
            }
            else {
                category = c.getString(c.getColumnIndex(Fields.NAME));
                holder.textVIewCategory.setText("Category name: " + category);
                holder.categoryLayout.setVisibility(View.VISIBLE);
            }
            c.close();
        }
    }

    @Override
    public int getItemCount() {
        return elements.size();
    }


    public class ElementViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener
    {
        private final TextView textViewName;
        private final TextView textViewPrice;
        private final TextView textVIewCategory;
        private final LinearLayout categoryLayout;

        public ElementViewHolder(View view){
            super(view);

            view.setOnClickListener(this);
            textViewName = (TextView) view.findViewById(R.id.textView_menu_name);
            textViewPrice = (TextView) view.findViewById(R.id.textView_menu_price);
            textVIewCategory = (TextView)itemView.findViewById(R.id.textView_menu_category);
            categoryLayout = (LinearLayout)itemView.findViewById(R.id.layout_menu_category);
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface  OnItemClickListener{
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener)
    {
        this.mItemClickListener = mItemClickListener;
    }
}
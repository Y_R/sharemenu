package me.android.yrusanov.sharemenu_out.tables;

/**
 * Created by y.rusanov on 2017-7-16.
 */

public class Product {

    String name;
    int id;
    Double price;
    String description;
    int idCategory;
    int check;

    public int isCheck() {
        return check;
    }

    public void setCheck(int check) {
        this.check = check;
    }

    public Product(String name, String description, Double price, int idCategory) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.idCategory = idCategory;
    }
    public Product(String name, Double price, int idCategory, int check) {
        this.name = name;
        this.price = price;
        this.check = check;
        this.idCategory = idCategory;
    }

    public Product(int id,String name, Double price, int idCategory, int check) {
        this.name = name;
        this.price = price;
        this.check = check;
        this.idCategory = idCategory;
        this.id = id;
    }

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

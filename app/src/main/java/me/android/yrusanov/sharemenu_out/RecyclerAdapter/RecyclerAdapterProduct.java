package me.android.yrusanov.sharemenu_out.RecyclerAdapter;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import me.android.yrusanov.sharemenu_out.ContentProviderApp;
import me.android.yrusanov.sharemenu_out.DB.Fields;
import me.android.yrusanov.sharemenu_out.R;
import me.android.yrusanov.sharemenu_out.tables.Product;

public class RecyclerAdapterProduct extends RecyclerView.Adapter<RecyclerAdapterProduct.ElementViewHolder>{

    OnItemClickListener mItemClickListener;
    ContentResolver resolver;
    ArrayList<Product> elements;
    Product product;
    String path;
    public String language;

    static final Uri CONTENT_URL = Uri.parse(String.valueOf(ContentProviderApp.CONTENT_URI_PRODUCT));



    public RecyclerAdapterProduct(Context _context){

        elements = new ArrayList<>();
        String contentName;
        try {
            //elements = populateBeer.selectAllByCategory(_IdCategory);
            //elements = populateBeer.populateAll();
            resolver = _context.getContentResolver();
            Cursor cursor = resolver.query(CONTENT_URL,null,null,null,null);
            if(cursor != null){
                cursor.moveToFirst();
                do {
                    elements.add(new Product(cursor.getString(1), cursor.getDouble(3), cursor.getInt(4), cursor.getInt(5)));
                } while (cursor.moveToNext());
            }

        }
        catch(ArrayIndexOutOfBoundsException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public ElementViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_product, parent, false);
        return new ElementViewHolder(rowView);
    }

    public void filter(Context _context, String filter){
        resolver = _context.getContentResolver();
        elements.clear();
        String likeClause = ""+Fields.NAME+" LIKE '%"+filter+"%'";
        Cursor cursor = resolver.query(CONTENT_URL,null,likeClause,null,null);
        if(cursor != null){
            cursor.moveToFirst();
            do {
                elements.add(new Product(cursor.getString(1), cursor.getDouble(3), cursor.getInt(4), cursor.getInt(5)));
            } while (cursor.moveToNext());
        }
    }

    @Override
    public void onBindViewHolder(ElementViewHolder holder, int position) {

        if(elements != null) {
            product = (Product) elements.get(position);
            final int pos = position;

            holder.textViewName.setText(product.getName());
            holder.textViewPrice.setText(String.valueOf(product.getPrice()));
            holder.checkBox.setOnCheckedChangeListener(null);
            Boolean flag = (product.isCheck() == 1);
            holder.checkBox.setSelected(flag);
            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                void updateRecordStatus(Product _product,CompoundButton buttonView){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(Fields.STATUS, _product.isCheck());
                    resolver.update(CONTENT_URL, contentValues, Fields.NAME + "='" + _product.getName()+"'", null);
                }

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        elements.get(pos).setCheck(1);
                    }else {
                        elements.get(pos).setCheck(0);
                    }
                    updateRecordStatus(elements.get(pos),buttonView);
                }
            });
            flag = (product.isCheck() == 1);
            holder.checkBox.setChecked(flag);
       }
    }

    @Override
    public int getItemCount() {
        return elements.size();
    }


    public class ElementViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener
    {
        private final TextView textViewName;
        private final TextView textViewPrice;
        private final CheckBox checkBox;
        //private final TextView textViewDescription;
        private ImageView imageViewProfile;

        public ElementViewHolder(View view){
            super(view);

            view.setOnClickListener(this);
            textViewName = (TextView) view.findViewById(R.id.textView_product_name);
            textViewPrice = (TextView) view.findViewById(R.id.textView_product_price);
            checkBox = (CheckBox) view.findViewById(R.id.checkBox);
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface  OnItemClickListener{
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener)
    {
        this.mItemClickListener = mItemClickListener;
    }
}
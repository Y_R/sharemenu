package me.android.yrusanov.sharemenu_out.restFB;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.restfb.BinaryAttachment;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.exception.FacebookOAuthException;
import com.restfb.types.FacebookType;

import java.io.ByteArrayOutputStream;

/**
 * Created by y.rusanov on 2017-7-26.
 */

public class FBPost extends java.lang.Thread{

    String message;
    Bitmap image;
    Context context;
    int type;
    FacebookClient fbClient;
    ProgressBar progressBar;
    Handler handler = new Handler();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public void run() {
        this.post();
        Message msg = Message.obtain();
        msg.obj = message;
        msg.setTarget(h);
        msg.sendToTarget();
        //h.sendMessage(msg);
    }

    private byte[] getByteArrImage(Bitmap _bitmap){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        _bitmap.compress(Bitmap.CompressFormat.PNG, 0, baos);
        byte[] imageByte = baos.toByteArray();
        return imageByte;
    }

    public void startThread(Bitmap image, String message, Context context){
        this.image = image;
        this.message = message;
        this.context = context;
        FBPost thread = new FBPost();
        thread.setMessage(message);
        thread.setImage(image);
        thread.setContext(context);
        thread.start();
    }

    public void startThread(Bitmap image, String message, Context context, ProgressBar progressBar){
        this.image = image;
        this.message = message;
        this.context = context;
        this.progressBar = progressBar;
        FBPost thread = new FBPost();
        thread.setMessage(message);
        thread.setImage(image);
        thread.setProgressBar(progressBar);
        thread.setContext(context);
        thread.start();
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public FBPost(){
        //token
        fbClient = new DefaultFacebookClient("EAAXlQYZCwlDUBADdtAezHCZC93NNNfAHZChaVE7HqNKsOYVhWZBadUV2FcB2oH31NyrjcxMXnu9rO4UsjG7ZCxb5mV7oLdmYJ8YeiPkXG9SkJBQKTiCYRZAxDnlRa7iBbMTwiO8MiJu2ZBtPm0q59AxHjtKQV8GclszQ3teRV8YoAZDZD");
    }

    private void post(){
        FacebookType response;

        //fbClient.obtainExtendedAccessToken("1748266158800198","77b76ed935f01cd428fd1101e644e04a");
        fbClient.obtainExtendedAccessToken("1659444634096693","088e963c1114c27f3134d7c1bbdde0a2");
        try{
            response = fbClient.publish("me/photos", FacebookType.class, BinaryAttachment.with("beer.png",getByteArrImage(getImage())), Parameter.with("message",getMessage()));
            type = 0;
        }
        catch (FacebookOAuthException exception){
            System.out.println("exception: " + exception.getMessage());
            message = exception.getMessage();
            type = 1;
        }
        //FacebookType response = fbClient.publish("me/feed", FacebookType.class,Parameter.with("message","test"));
    }

    Handler h = new Handler(){
        @Override
        public void handleMessage(Message msg){
            if(type == 0){
                Toast.makeText(context, "Successfully posted",Toast.LENGTH_LONG).show();
            }
            else if(type == 1){
                String str = (String)msg.obj;
                Toast.makeText(context, "Connect with Your Provider: " + str,Toast.LENGTH_LONG).show();
            }
            progressBar.setVisibility(View.GONE);
        }
    };


}
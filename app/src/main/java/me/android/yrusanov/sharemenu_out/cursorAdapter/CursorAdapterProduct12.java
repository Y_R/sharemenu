package me.android.yrusanov.sharemenu_out.cursorAdapter;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import me.android.yrusanov.sharemenu_out.ContentProviderApp;
import me.android.yrusanov.sharemenu_out.DB.Fields;
import me.android.yrusanov.sharemenu_out.R;
import me.android.yrusanov.sharemenu_out.tables.Product;

/**
 * Created by y.rusanov on 2017-8-12.
 */

public class CursorAdapterProduct12 extends RecyclerViewCursorAdapter<CursorAdapterProduct12.ElementViewHolder>
        implements View.OnClickListener {

    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;
    ContentResolver resolver;
    static final Uri CONTENT_URL = Uri.parse(String.valueOf(ContentProviderApp.CONTENT_URI_PRODUCT));

    public CursorAdapterProduct12(final Context context)
    {
        super();
        resolver = context.getContentResolver();

        this.layoutInflater = LayoutInflater.from(context);
    }

    public void setOnItemClickListener(final OnItemClickListener onItemClickListener)
    {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ElementViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType)
    {
        final View view = this.layoutInflater.inflate(R.layout.recycler_view_product, parent, false);
        view.setOnClickListener(this);

        return new ElementViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ElementViewHolder holder, final Cursor cursor)
    {
        //holder.bindData(cursor);
        holder.textViewName.setText(cursor.getString(cursor.getColumnIndex(Fields.NAME)));

        holder.textViewPrice.setText(String.valueOf(cursor.getDouble(cursor.getColumnIndex(Fields.PRICE))));
        String[] projection = {Fields.NAME};
        Cursor c = resolver.query(ContentProviderApp.CONTENT_URI_CATEGORY,projection,""+Fields.ID+" = "+cursor.getInt((cursor.getColumnIndex(Fields.IDCATEGORY)))+"",null,null);
        c.moveToFirst();
        holder.textVIewCategory.setText(c.getString(c.getColumnIndex(Fields.NAME)));
        c.close();

        holder.checkBox.setOnCheckedChangeListener(null);
        Boolean flag = (cursor.getInt(cursor.getColumnIndex(Fields.STATUS)) == 1);
        holder.checkBox.setSelected(flag);
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            void updateRecordStatus(Product _product,CompoundButton buttonView){
                ContentValues contentValues = new ContentValues();
                contentValues.put(Fields.STATUS, _product.isCheck());
                resolver.update(CONTENT_URL, contentValues, Fields.NAME + "='" + _product.getName()+"'", null);
            }

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Product product;
                if(isChecked){
                    cursor.moveToPosition(holder.getAdapterPosition());
                    product = new Product(cursor.getString(1), cursor.getDouble(3), cursor.getInt(4), 1);
                    //elements.get(pos).setCheck(1);
                }else {
                    cursor.moveToPosition(holder.getAdapterPosition());
                    product = new Product(cursor.getString(1), cursor.getDouble(3), cursor.getInt(4), 0);
                    //elements.get(pos).setCheck(0);
                }
                updateRecordStatus(product,buttonView);
            }
        });
        flag = (cursor.getInt(cursor.getColumnIndex(Fields.STATUS)) == 1);
        holder.checkBox.setChecked(flag);
    }

    @Override
    public void onClick(View v) {

    }

     /*
     * View.OnClickListener
     */

    /*
    @Override
    public void onClick(final View view)
    {
        if (this.onItemClickListener != null)
        {
            final RecyclerView recyclerView = (RecyclerView) view.getParent();
            final int position = recyclerView.getChildLayoutPosition(view);
            if (position != RecyclerView.NO_POSITION)
            {
                final Cursor cursor = this.getItem(position);
                this.onItemClickListener.onItemClicked(cursor);
            }
        }
    }
*/
    public static class ElementViewHolder extends RecyclerView.ViewHolder
    {
        private final TextView textViewName;
        private final TextView textViewPrice;
        private final TextView textVIewCategory;
        private final CheckBox checkBox;

        public ElementViewHolder(final View itemView)
        {
            super(itemView);
            textViewName = (TextView)   itemView.findViewById(R.id.textView_product_name);
            textViewPrice = (TextView) itemView.findViewById(R.id.textView_product_price);
            textVIewCategory = (TextView)itemView.findViewById(R.id.textView_product_category);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);

        }

        public void bindData(final Cursor cursor) {
            final String name = cursor.getString(cursor.getColumnIndex("name"));
        }
    }

    public interface OnItemClickListener
    {
        void onItemClicked(Cursor cursor);
    }

}
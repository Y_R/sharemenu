package me.android.yrusanov.sharemenu_out.cursorAdapter;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import me.android.yrusanov.sharemenu_out.ContentProviderApp;
import me.android.yrusanov.sharemenu_out.DB.Fields;
import me.android.yrusanov.sharemenu_out.R;
import me.android.yrusanov.sharemenu_out.layout.EditFragment;
import me.android.yrusanov.sharemenu_out.tables.Product;

/**
 * Created by y.rusanov on 2017-8-17.
 */

public class CursorAdapterProduct extends CursorRecyclerAdapter<CursorAdapterProduct.ElementViewHolder>{

    ContentResolver resolver;
    static final Uri CONTENT_URL = Uri.parse(String.valueOf(ContentProviderApp.CONTENT_URI_PRODUCT));
    private String categoryName = "";
    int marginTop = 0;
    public boolean editMode = false;
    public FragmentManager fragmentManager;
    View view;
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    private LayoutInflater layoutInflater;
    private Context mContext;

    public FragmentManager f_manager;
    public CursorAdapterProduct(Cursor cursor, Context context) {

        super(cursor);
        resolver = context.getContentResolver();
        this.mContext = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public CursorAdapterProduct(Cursor cursor, Context context,FragmentManager f_manager) {

        super(cursor);
        resolver = context.getContentResolver();
        this.mContext = context;
        this.f_manager = f_manager;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public void onBindViewHolder(final ElementViewHolder holder, final Cursor cursor) {
        holder.textViewName.setText(cursor.getString(cursor.getColumnIndex(Fields.NAME)));
        //holder.textViewPrice.setText(String.valueOf(cursor.getDouble(cursor.getColumnIndex(Fields.PRICE))));
        holder.textViewPrice.setText(String.format("%.2f",cursor.getDouble(cursor.getColumnIndex(Fields.PRICE))));

        //String[] projection = {Fields.NAME};
        //Cursor c = resolver.query(ContentProviderApp.CONTENT_URI_CATEGORY,projection,""+Fields.ID+" = "+cursor.getInt((cursor.getColumnIndex(Fields.IDCATEGORY)))+"",null,null);
        //c.moveToFirst();
        /*
        if(categoryName.equals(c.getString(c.getColumnIndex(Fields.NAME)))){
            holder.textVIewCategory.setText("");
            holder.categoryLayout.setVisibility(View.GONE);
        }
        else {
            categoryName = c.getString(c.getColumnIndex(Fields.NAME));
            holder.textVIewCategory.setText("Category name: " + categoryName);
            holder.categoryLayout.setVisibility(View.VISIBLE);
        }
        */
        //c.close();
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) holder.layout_product.getLayoutParams();
        if(editMode){
            params.setMargins(0,40,0,0);
            params.height = 150;
            holder.layout_product.setBackgroundColor(Color.rgb(226, 226, 226));
            holder.checkBox.setVisibility(View.GONE);

        }
        else{

            params.setMargins(0,0,0,0);
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.layout_product.setBackgroundColor(Color.WHITE);
        }
        holder.checkBox.setOnCheckedChangeListener(null);
        Boolean flag = (cursor.getInt(cursor.getColumnIndex(Fields.STATUS)) == 1);

        holder.layout_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editMode){
                    EditFragment editFragment = new EditFragment();
                    Bundle bundle = new Bundle();
                    cursor.moveToPosition(holder.getAdapterPosition());
                    bundle.putInt("ID", cursor.getInt((cursor.getColumnIndex(Fields.ID))));
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentManager.popBackStack();
                    editFragment.setArguments(bundle);
                    fragmentTransaction.replace(R.id.content, editFragment).addToBackStack("tag");
                    fragmentTransaction.commit();
                }
            }
        });

        if(holder.checkBox.getVisibility() == View.VISIBLE){
            holder.checkBox.setSelected(flag);
            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                void updateRecordStatus(Product _product, CompoundButton buttonView){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(Fields.STATUS, _product.isCheck());
                    resolver.update(CONTENT_URL, contentValues, Fields.NAME + "='" + _product.getName()+"'", null);
                }

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Product product;
                    if(isChecked){
                        cursor.moveToPosition(holder.getAdapterPosition());
                        product = new Product(cursor.getString(1), cursor.getDouble(3), cursor.getInt(4), 1);
                        //elements.get(pos).setCheck(1);
                    }else {
                        cursor.moveToPosition(holder.getAdapterPosition());
                        product = new Product(cursor.getString(1), cursor.getDouble(3), cursor.getInt(4), 0);
                        //elements.get(pos).setCheck(0);
                    }
                    updateRecordStatus(product,buttonView);
                }
            });
            flag = (cursor.getInt(cursor.getColumnIndex(Fields.STATUS)) == 1);
            holder.checkBox.setChecked(flag);
        }
    }

    @Override
    public ElementViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.recycler_view_product, parent, false);
        return new ElementViewHolder(view);
    }

    public static class ElementViewHolder extends RecyclerView.ViewHolder
    {
        private final TextView textViewName;
        private final TextView textViewPrice;
        private final TextView textVIewCategory;
        private final CheckBox checkBox;
        private final LinearLayout categoryLayout;
        private final LinearLayout layout_product;
        public ElementViewHolder(final View itemView)
        {
            super(itemView);
            textViewName = (TextView)   itemView.findViewById(R.id.textView_product_name);
            textViewPrice = (TextView) itemView.findViewById(R.id.textView_product_price);
            textVIewCategory = (TextView)itemView.findViewById(R.id.textView_product_category);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
            categoryLayout = (LinearLayout)itemView.findViewById(R.id.layout_product_category);
            layout_product =(LinearLayout)itemView.findViewById(R.id.layout_product);
        }
    }
}
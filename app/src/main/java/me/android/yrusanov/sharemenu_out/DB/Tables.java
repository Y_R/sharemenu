package me.android.yrusanov.sharemenu_out.DB;

/**
 * Created by y.rusanov on 2017-7-16.
 */

public class Tables {
    public static final String TABLE_PRODUCT = "PRODUCT";
    public static final String TABLE_MENU = "MENU";
    public static final String TABLE_CATEGORY = "CATEGORY";
}
